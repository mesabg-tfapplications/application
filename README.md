
# Server Application Module

This module is capable to deploy a server application on an existing ECS cluster and connected it to a CDN domain or CloudMap public namespace

Module Input Variables
----------------------

- `environment` - environment name
- `application_name` - application_name
- `project_name` - general project name
- `cluster_name` - cluster name
- `image` - registry image
- `subnet_ids` - subnets identifiers
- `security_group` - security group to be applied on the application
- `log_group` - to store the logs
- `namespace_id` - cloud map namespace identifier
- `namespace_name` - cloud map namespace domain name
- `max_capacity` - default 4
- `cpu` - cpu units to allocate (default 256)
- `memory` - memory to allocate (default 512)

Usage
-----

```hcl
module "application" {
  source                  = "git::https://gitlab.com/mesabg-tfapplications/server-application.git"

  environment             = "environment"
  application_name        = "someapp"
  project_name            = "name"
  cluster_name            = "cluster"
  image                   = "registryimage:latest"

  subnet_ids              = ["subnet-xxxx"]
  security_group          = "sg-xxxx"
  auto_scaling_role_arn   = "iam::role"
  task_execution_role_arn = "iam::role"
  log_group               = "/some/group"

  namespace_id            = "ns-xxxx"
  namespace_name          = "default.domain"
}
```

Outputs
=======

 - `dns_name` - Domain where the application is hosted


Authors
=======
##### Moisés Berenguer <moises.berenguer@gmail.com>
